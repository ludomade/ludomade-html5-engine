# Ludomade Game Engine (LGE) #

HTML5 + JS game engine built for all internal web-games. The engine takes some cues from Quintus, while still allowing for flexibility and extensibility. All underlying systems are still accessible. Pixi.js and Howler are the drivers for the audio/visual aspects.

### Versioning ###

#### v1.1 ####
Versioned for commits prior to 03 Dec 2015. Working core engine with minor updates for audio.

#### Development (v1.2beta - untagged) ####
* Updates the L.Utils.collisions method to use a different hitbox offset calculation. (BREAKING)
* Adds Z-Index ordering support for tiled object layers.

### Examples ###

* [Jared's LD33 entry - infinite runner](https://github.com/JaredSartin/LD33)

### How do I get set up? ###

* Clone the repo
* `npm install`
* `gulp build` to build a new version

Do not push to master unless you are sure it is stable and doesn't change current API without proper fallback! You can include local copies of the framework into your game project via bower or NPM.

### How do I include the engine? ###

* Edit your package.json:
```
  "dependencies": {
    "ludomade-game-engine": "git+ssh://git@bitbucket.org/ludomade/ludomade-html5-engine.git#v1.1"
  }
```
* `require("ludomade-game-engine");`
* Make games!

### Usage ###

Please see the examples and to get started on installation and usage. The game templates will be setup for single builds and there will eventually be a starter repo to base new games off of!

### License ###
GPLv3 - see LICENSE file.