var Class = require("class-js2");
var Easing = require("easing-js");

// Need to be a class?
module.exports = Class.create({
  Easing: Easing,
  lerp: function(t, a, b) {
    return (1-t) * a + t * b;
  },
  // Returns a random number from min to max (inclusive)
  randInt: function(min, max) {
    if(min !== undefined && max === undefined) {
      max = min;
      min = 0;
    }
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },

  // Move to math?
  clamp: function(val, min, max) {
    if(val > max) return max;
    if(val < min) return min;
    return val;
  },

  // DEPRECATED - Use L.Matrix.vec2...
  vector: function vector(x, y) {
    console.log("L.Utils.vector is no longer supported - please use L.Matrix.vec2!");
    if(typeof y !== "number") debugger;
    return {
      x:x,
      y:y,

      add: function(vector) {
        this.x += vector.x;
        this.y += vector.y;
      }
    };
  },

  // Move to math?
  sign: function(x) {
    x = +x; // convert to a number
    if (x === 0 || isNaN(x)) {
      return x;
    }
    return x > 0 ? 1 : -1;
  },

  // Move to math?
  degToRad: function(deg) { return deg * (Math.PI/180) },
  radToDeg: function(rad) { return rad * (180/Math.PI) },

  // Very specific - get rid of or move to a 2D module?
  accelerate: function (object, rotate) {
    if (!object.velocity) {
      object.velocity = this.vector(0,0);
    }
    object.velocity.add(object.acceleration);
    object.position.x += object.velocity.x;
    object.position.y += object.velocity.y;
    if(rotate) object.rotation = Math.PI/2 * Math.sin(object.velocity.y/100);
  },

  collisions: function(ra1, ra2, cb) {
    var len1 = ra1.length;
    while (len1--) {
      var len2 = ra2.length;
      while (len2--) {
        this.collision(ra1[len1], ra2[len2], cb);
      }
    }
  },

  collision: function(r1, r2, cb) {
    if(!r1.hitbox) {
      r1.hitbox = {
        width: r1.width * r1.scale.x,
        height: r1.height * r1.scale.y,
        cx: (r1.scale.x * r1.width * r1.anchor.x),
        cy: (r1.scale.y * r1.height * r1.anchor.y)
      };
    }

    if(!r2.hitbox) {
      r2.hitbox = {
        width: r2.width * r2.scale.x,
        height: r2.height * r2.scale.y,
        cx: (r2.scale.x * r2.width * r2.anchor.x),
        cy: (r2.scale.y * r2.height * r2.anchor.y)
      };
    }

    var r1x = r1.x - r1.hitbox.cx;
    var r1y = r1.y - r1.hitbox.cy;

    var r2x = r2.x - r2.hitbox.cx;
    var r2y = r2.y - r2.hitbox.cy;

    if(!(r2x > (r1x + r1.hitbox.width) || (r2x + r2.hitbox.width) < r1x || r2y > (r1y + r1.hitbox.height) || (r2y + r2.hitbox.height) < r1y)) {
      cb(r1, r2);
    }
  },
});
