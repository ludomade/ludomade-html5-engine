var Class = require("class-js2");
var PIXI = require("pixi.js");

module.exports = Class.extend(PIXI.Container, {
  type: "Scene",
  stage: undefined,
  backgroundColor: "transparent",

  overlays: {},

  paused: true,
  className: undefined,
  name: "scene",

  constructor: function() {
    this.PIXI =  L.PIXI;
    this._super([this.backgroundColor]);
  
    if(this.setup !== undefined)
      this.setup();

    this.rohBind = this.reflowOrientationHandler.bind(this);
    this.rrhBind = this.reflowResizeHandler.bind(this);
  },

  pause: function() {
    if(this.onPause) this.onPause();
    this.paused = true;
    L.Sound.pauseMusic();
  },
  resume: function() {
    if(this.onResume) this.onResume();
    this.paused = false;
    L.Sound.resumeMusic();
  },
  isPaused: function() {
    return this.paused;
  },

  swapIn: function(data) {
    L.Device.on("orientationchange", this.rohBind);
    L.Device.on("sizechange", this.rrhBind);

    if(this.onSwapIn) this.onSwapIn(data);
  },

  swapOut: function() {
    L.Device.off("orientationchange", this.rohBind);
    L.Device.off("sizechange", this.rrhBind);

    this.removeChildren();

    if(this.onSwapOut) return this.onSwapOut();
  },

  update: function(dt) {
    if(this.onUpdate !== undefined)
      this.onUpdate(dt);
  },

  reflowOrientationHandler: function(e) {
    if(this.orientationReflow) this.orientationReflow(e);
  },
  reflowResizeHandler: function(e) {
    if(this.resizeReflow) this.resizeReflow(e);
  },
});
