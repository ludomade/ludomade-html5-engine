var Class = require("class-js2");

module.exports = Class.create({
  fromImage: function(imageName) {
    var texture = L.PIXI.Texture.fromImage("images/" + imageName);
    return new L.PIXI.Sprite(texture);
  },
  fromSheet: function(imageName, frameData) {
    return new L.AnimatedSprite("images/" + imageName, frameData);
  },
  tilingFromImage: function(imageName) {
    var repeaterTexture = PIXI.Texture.fromImage("images/" + imageName);
    return new PIXI.extras.TilingSprite(repeaterTexture, repeaterTexture.width, repeaterTexture.height);
  }
});
