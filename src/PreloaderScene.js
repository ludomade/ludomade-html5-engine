var preloaderOverlay = require("./preloader.js");
var Scene = require("./Scene.js");
var _ = require("underscore");

module.exports = Scene.extend({
	name: "preloader",

	setup: function() {
		this.preloaderOverlay = new preloaderOverlay();
	},

	customPreloader: function(preloader) {
		this.preloaderOverlay = preloader;
	},

	onSwapIn: function() {
		L.Analytics.screen("Preloader");
		this.preloaderOverlay.addToScene(this);
		this.startLoad();
	},

	onSwapOut: function() {
		L.Sound.off("onload");
		L.DataLoader.off("onload");
		L.FontLoader.off("onload");
		this.preloaderOverlay.remove()
	},

	startLoad: function() {
		L.Analytics.event("boot", "loading");
		L.Sound.on("onload", _.bind(this.updateBar, this));
		L.DataLoader.on("onload", _.bind(this.updateBar, this));
		L.FontLoader.on("onload", _.bind(this.updateBar, this));

		if(L.preload) {
			L.Sound.load(L.preload.sound);
			L.DataLoader.load(L.preload.data);
			L.FontLoader.load(L.preload.font);
		}

		this.updateBar();
	},

	updateBar: function() {
		var soundStatus = L.Sound.status();
		var dataStatus = L.DataLoader.status();
		var fontStatus = L.FontLoader.status();

		var complete = soundStatus.complete + dataStatus.complete + fontStatus.complete; 
		var total = soundStatus.total + dataStatus.total + fontStatus.total; 
		this.preloaderOverlay.updatePercent((complete/total) * 100);
		if(complete == total) {
			L.Analytics.event("boot", "loading complete");
			setTimeout(_.bind(function(){
				this.emit("finished");
			}, this), 1000);
		}
	}
});
