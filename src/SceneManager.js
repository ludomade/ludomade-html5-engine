var Class = require("class-js2");
var EventEmitter = require("eventemitter3");
var _ = require("underscore");

module.exports = Class.create({
  mixins: [EventEmitter],

  currentScene: undefined,
  scenes: {},
  color: 0xFFFFFF,
  el: undefined,

  constructor: function() {
    EventEmitter.call(this);
    this.stage = L.PixiRenderer.renderer,
    this.el = document.querySelector('.game-container');
    if(!this.el) {
      throw "Cannot find container to place the renderer! Looking for '.game-container'";
    }
    this.el.appendChild(this.stage.view);

    L.Device.on("focuschange", _.bind(this.pauseCurrentScene, this));
    // L.Device.on("sizechange", _.bind(this.resizeCurrentScene, this));
    // L.Device.on("orientationchange", _.bind(this.rotateCurrentScene, this));
  },

  addScene: function(scene) {
    if(this.scenes[scene.name] !== undefined) {
      console.log("You have tried to add a scene with a name already in use [" + scene.name + "].");
      console.log("Please ensure you have unique scene names and only add them to the manager once!");
      return false;
    }

    this.scenes[scene.name] = scene;
    return true;
  },

  setCurrentScene: function(sceneName, data) {
    var nextScene = this.scenes[sceneName];

    if(!nextScene) throw new Error("Scene '" + sceneName + "' not found. Add it via L.SceneManager.addScene() or verify the spelling of your scene names.");

    if(this.currentScene !== undefined) this.currentScene.swapOut();

    this.currentScene = nextScene;
    nextScene.swapIn(data);

    L.Device.verifyScreenSetup();

    this.emit("scenechange");
  },

  renderCurrentScene: function() {
    if(this.currentScene !== undefined)
      this.stage.render(this.currentScene);
  },

  updateCurrentScene: function(dt) {
    if(this.currentScene !== undefined)
      this.currentScene.update(dt);
  },

  pauseCurrentScene: function() {
    if(this.currentScene !== undefined && !L.Device.focused) {
      this.currentScene.pause();
    }
  },
});
