var Class = require("class-js2");
var EventEmitter = require("eventemitter3");
var isMobile = require("ismobilejs");
var ifvisible = require("ifvisible.js");
var _ = require("underscore");
var screamLib = require("scream");
var brimLib = require("brim");

module.exports = Class.create({
  mixins: [EventEmitter],

  width: screen.width,
  height: screen.height,
  windowHeight: window.innerHeight,
  windowWidth: window.innerWidth,
  mobile: true,
  focused: true,
  rotation: 0,
  orientation: "landscape",
  pixelRatio: window.devicePixelRatio || 1,

  constructor: function() {
    EventEmitter.call(this);
    // might need better size checks:
    // http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript
    this.mobile = isMobile.any;
    this.rotation = window.orientation;
    this.orientation = this.readOrientation();
    this.focused = ifvisible.now();

    var timeout = 300;

    var sizeWatcher = _.bind(this.onSizeChange, this);
    window.addEventListener('resize', _.debounce(sizeWatcher, timeout));

    var orientationWatcher = _.bind(this.onOrientationChange, this);
    window.addEventListener('orientationchange', _.debounce(orientationWatcher, timeout));

    var focusWatcher = _.bind(this.onFocusChange, this);
    ifvisible.on("blur", _.debounce(focusWatcher, timeout));
    ifvisible.on("focus", _.debounce(focusWatcher, timeout));

    if(L.settings.iosFullscreen && /iPad|iPhone|iPod/.test(navigator.platform) && !/CriOS/.test(navigator.userAgent)) {
      window.removeEventListener('orientationchange');
      window.removeEventListener('resize');

      this.iosFullscreen = true;

      var scream = gajus.Scream({
        width: {
          portrait: 320,
          landscape: 640
        }
      });
      var brim = gajus.Brim({
        viewport: scream
      });

      scream.on("orientationchangeend", this.onScreamOrientationChange.bind(this, scream));
      scream.on("viewchange", this.handleScreamViewChange.bind(this, scream));
      this._scream = scream;
      this._brim = brim;
    } else {
      var brimMask = document.querySelector("#brim-mask")
      if(brimMask) document.body.removeChild(brimMask);
    }
  },

  screen: function() {
    return {
      width: this.width,
      height: this.height,
      orientation: this.orientation,
      rotation: this.orientation,
      pixelRatio: this.pixelRatio,
      mobile: this.mobile
    };
  },
  window: function() {
    return {
      width: this.windowWidth,
      height: this.windowHeight,
      orientation: this.orientation,
      rotation: this.orientation,
      pixelRatio: this.pixelRatio,
      mobile: this.mobile
    };
  },

  verifyScreenSetup: function() {
    if(L.settings.iosFullscreen && this._scream) {
      this.handleScreamViewChange(this._scream);

      if(!this.expectedFullscreen()) {
        this.emit("sizechange");
        // No helper on Brim.
        this._brim._treadmill();
        this._brim._main();
        this._brim._mask();
      }
    }
  },

  onScreamOrientationChange: function(scream) {
    this.isMinimal = scream.isMinimalView();
    this.windowHeight = window.innerHeight;
    this.windowWidth = window.innerWidth;
    this.rotation = window.orientation;
    this.orientation = scream.getOrientation();

    this.emit("orientationchange");

    if(!this.isMinimal) this.emit("notminimalios");
    else this.emit("isminimalios");
  },
  handleScreamViewChange: function(scream) {
    this.isMinimal = scream.isMinimalView();
    this.windowHeight = window.innerHeight;
    this.windowWidth = window.innerWidth;

    if(!this.isMinimal) this.emit("notminimalios");
    else this.emit("isminimalios");
  },

  onOrientationChange: function() {
    this.windowHeight = window.innerHeight;
    this.windowWidth = window.innerWidth;
    this.rotation = window.orientation;
    this.orientation = this.readOrientation();
    this.emit("orientationchange");
  },
  onFocusChange: function() {
    this.focused = ifvisible.now();
    this.emit("focuschange");
  },
  onSizeChange: function() {
    this.windowHeight = window.innerHeight;
    this.windowWidth = window.innerWidth;
    this.rotation = window.orientation;
    this.orientation = this.readOrientation();
    this.emit("sizechange");
  },

  readOrientation: function() {
    if(this.mobile === false) {
      if(window.innerHeight >= window.innerWidth)
        return "portrait";
      else
        return "landscape";
    }
    if (Math.abs(window.orientation) === 90)
      return "landscape";
    else
      return "portrait";
  },

  expectedRotation: function() {
    if(L.settings.layout.orientation !== undefined) {
      var expectedRotation = true
      if(this.mobile && this.orientation !== L.settings.layout.orientation) {
        expectedRotation = false
      }

      return expectedRotation;
    };

    return true;
  },

  expectedSize: function() {
    if(L.settings.layout.minWidth !== undefined || L.settings.layout.minHeight !== undefined) {
      var expectedSize = true;
      if(!this.mobile &&
         ((L.settings.layout.minHeight !== undefined && L.Device.windowHeight < L.settings.layout.minHeight) ||
          (L.settings.layout.minWidth !== undefined && L.Device.windowWidth < L.settings.layout.minWidth))) {
        expectedSize = false;
      }
    };

    return true;
  },

  expectedFullscreen: function() {
    if(this.iosFullscreen) {
      if(!this.isMinimal) return false;
      return true;
    };

    return true;
  },
});
