var Class = require("class-js2");
var _ = require("underscore");
require("requestanimationframe");

module.exports = Class.create({
  constructor: function() {
    this.setup();
    this.runGameLoop();
  },

  runGameLoop: function() {
    var targetFPS = 60;

    var time;
    var gameLoop = function() {
      requestAnimationFrame(gameLoop);
      var now = new Date().getTime();
      var dt = now - (time || now);
      time = now;

      this.update(dt);
      this.draw(dt);
    }.bind(this);

    requestAnimationFrame(gameLoop);
  }
});
