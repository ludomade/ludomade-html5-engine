var Class = require("class-js2");
var EventEmitter = require("eventemitter3");

module.exports = Class.create({
  mixins: [EventEmitter],

  _google: false,
  _flurry: false,
  _log: false,

  constructor: function() {
    if(L.settings.debug) this._log = true;
    if(!L.settings.analytics) return;
    if(L.settings.analytics.google) {
      console.log("Initializing Google Analytics...");
      this._google = true;
      this._googleReady = false;
      this._addGoogleCode(L.settings.analytics.google);
    }
    if(L.settings.analytics.flurry) {
      console.log("Initializing Flurry Analytics...");
      this._flurry = true;
      this._flurryReady = false;
      this._flurryStarted = false;
      this._addFlurryCode(L.settings.analytics.flurry);
    }
  },

  screen: function(screenName) {
    if(this._log) console.log("[Debug][Analytics] Screen: " + screenName);
    if(this._google) {
      this._trackGoogle(["set", "screenName", screenName]);
    }
    this.screenview(screenName);
  },

  screenview: function(screenName) {
    if(this._log) console.log("[Debug][Analytics] ScreenView: " + screenName);
    if(this._google) {
      this._trackGoogle(["send", "screenview"]);
    }
    if(this._flurry) {
      this._trackFlurry([screenName]);
    }
  },

  event: function(eventCategory, eventAction, eventData, eventLabel) {
    if(this._log) {
      console.log("[Debug][Analytics] Event - Category: " + eventCategory);
      console.log("[Debug][Analytics] Event - Action: " + eventAction);
      console.log("[Debug][Analytics] Event - Data: " + eventData);
      console.log("[Debug][Analytics] Event - Label: " + eventLabel);
    }
    if(this._google) {
      this._trackGoogle(["send", "event", eventCategory, eventAction, eventLabel, eventData]);
    }
    if(this._flurry) {
      var data = {};
      if(eventCategory) data.category = JSON.stringify(eventCategory);
      if(eventAction) data.action = JSON.stringify(eventAction);
      if(eventLabel) data.label = JSON.stringify(eventLabel);
      if(eventData) data.data = JSON.stringify(eventData);

      this._trackFlurry([eventAction, data]);
    }
  },

  _addGoogleCode: function(trackingKey) {
    var gaScript = "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', '" + trackingKey + "', 'auto'); ga('send', 'pageview');"
    this._addTrackingCode(gaScript);
  },

  _addFlurryCode: function(trackingKey) {
    this._addTrackingCode(undefined, "https://cdn.flurry.com/js/flurry.js");
    this._startFlurry(trackingKey);
  },

  _startFlurry: function(trackingKey) {
    if(this._flurryReady) {
      FlurryAgent.startSession(trackingKey);
    } else if(window.FlurryAgent && typeof window.FlurryAgent == "function") {
      this._flurryReady = true;
      FlurryAgent.startSession(trackingKey);
      this._flurryStarted = true;
    } else {
      setTimeout(this._startFlurry.bind(this, trackingKey), 100);
    }
  },

  _addTrackingCode: function(codeString, src) {
    var s = document.createElement('script');
    s.setAttribute('type', 'text/javascript');
    if(src) s.src = src;
    if(codeString) s.text = codeString;
    document.body.appendChild(s);
  },

  _trackGoogle: function(args) {
    if(this._googleReady) {
      ga.apply(ga, args);
    } else if(window.ga && typeof window.ga == "function") {
      this._googleReady = true;
      ga.apply(ga, args);
    } else {
      setTimeout(this._trackGoogle.bind(this, args), 100);
    }
  },

  _trackFlurry: function(args) {
    if(this._flurryReady && this._flurryStarted) {
      FlurryAgent.logEvent.apply(FlurryAgent, args);
    } else {
      setTimeout(this._trackFlurry.bind(this, args), 100);
    }
  },
});
