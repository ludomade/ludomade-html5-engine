var Class = require("class-js2");
var EventEmitter = require("eventemitter3");
var _ = require("underscore");

module.exports = Class.create({
  mixins: [EventEmitter],
  renderer: undefined,

  constructor: function() {
    this.PIXI = L.PIXI,
    EventEmitter.call(this);
    console.log("Building PIXI renderer");
    var opts = {
      resolution: window.devicePixelRatio || 1
    };
    var width = L.settings.layout.width || window.innerWidth;
    var height = L.settings.layout.height || window.innerHeight;

    this.renderer = new L.PIXI.autoDetectRenderer(width, height, opts);
    this.renderer.view.style.margin = "0 auto";
    this.renderer.view.style.display = "inherit";

    switch(L.settings.layout.scaling) {
      case "fill":
      case "stretch":
      case "fit":
        L.Device.on("sizechange", _.bind(this.resizeRenderer, this));
        L.Device.on("orientationchange", _.bind(this.resizeRenderer, this));
        break;
    }
  },

  resizeRenderer: function() {
    var width = window.innerWidth;
    var height = window.innerHeight;

    switch(L.settings.layout.scaling) {
      case "fill":
        this.renderer.resize(width, height);
        this.emit("resizerenderer", {width: width, height: height});
      case "stretch":
        this.renderer.view.style.width = "100%";
        this.renderer.view.style.height = "100%";
        break;
      case "fit":
        this.renderer.view.style.maxWidth = "100vw";
        this.renderer.view.style.maxHeight = "100vh";
        var orientation = L.Device.orientation;
        if(L.Device.mobile === false) {
          if((L.Device.windowWidth / L.Device.windowHeight) >= (L.settings.layout.width / L.settings.layout.height))
            orientation = "landscape";
          else
            orientation = "portrait";
        }
        if(orientation == "landscape") {
          this.renderer.view.style.width = "auto";
          this.renderer.view.style.height = "100vh";
        } else {
          this.renderer.view.style.width = "100vw";
          this.renderer.view.style.height = "auto";
        }
        break;
    }
  },

  createStage: function(color) {
    return new L.PIXI.Stage(color);
  }
});
