var Class = require("class-js2");
var PIXI = require("pixi.js");

module.exports = Class.extend(PIXI.Container, {
  name: "layer",

  constructor: function() {
    this.PIXI = L.PIXI;
    this._super();
    if(this.setup) this.setup();
  },

  addToScene: function(stage) {
    this.stageRef = stage;
    this.stageRef.addChild(this);
    if(this.onAdd) this.onAdd();
  },

  remove: function() {
    if(this.onRemove) this.onRemove();
    this.removeChildren();
    if(!this.stageRef) return;
    this.stageRef.removeChild(this);
  }
});
