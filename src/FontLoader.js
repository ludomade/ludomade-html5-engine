var Class = require("class-js2");
var EventEmitter = require("eventemitter3");
var ffol = require("fontfaceonload");
var _ = require("underscore");

module.exports = Class.create({
  mixins: [EventEmitter],

  fontCount: 0,
  loadedFonts: 0,

  constructor: function() {
    EventEmitter.call(this);
  },

  load: function(fontList) {
    this.fontCount += fontList.length;

    for(i in fontList) {
      ffol(fontList[i], {
        success: _.bind(this.onLoad, this),
        error: function() { console.log("Font failed to load"); }
      });
    }
  },

  onLoad: function(e) {
    this.loadedFonts++;
    this.emit("onload", this.status());
  },

  onComplete: function() {
    this.emit("loadcomplete");
  },

  status: function() {
    return {
      total: this.fontCount,
      complete: this.loadedFonts
    };
  },
});
