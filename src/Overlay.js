var Class = require("class-js2");
var EventEmitter = require("eventemitter3");
var _ = require("underscore");

module.exports = Class.create({
  type: "Overlay",
  mixins: [EventEmitter],

  tagName: "div",
  className: undefined,
  name: "overlay",

  el: undefined,

  // false allows click/tap-through
  // true traps events from going to lower layers
  isolating: false,

  ui: {},

  constructor: function(opts) {
    EventEmitter.call(this);
    this.el = document.createElement(this.tagName);

    this._addClass(this.className);
    this._addClass("overlay-" + this.name.toLowerCase().replace(" ", "-"));
    // this._addClass("overlay-" + _.kebabCase(this.name));

    var width = L.settings.layout.width || window.innerWidth;
    var height = L.settings.layout.height || window.innerHeight;
    this.el.style.margin = "0 auto";
    this.el.style.width = width;
    this.el.style.height = height;
    this.el.style.position = "absolute";
    this.el.style.top = 0;
    this.el.style.left = 0;
    if(!this.isolating)
      this.el.style.pointerEvents = "none";

    switch(L.settings.layout.scaling) {
      case "fill":
      case "stretch":
      case "fit":
        L.Device.on("orientationchange", _.bind(this.resizeOverlay, this));
        L.Device.on("sizechange", _.bind(this.resizeOverlay, this));
        this.resizeOverlay();
        break;
    }

    if(this.setup !== undefined)
      this.setup();
  },

  setup: function() {
    // NOOP
  },

  addToScene: function(stage) {
    this.add();
  },

  add: function() {
    L.SceneManager.el.appendChild(this.el);
    if(this.onAdd)
      this.onAdd();
  },
  remove: function() {
    if(this.onRemove)
      this.onRemove();
    // L.SceneManager.el.removeChild(this.el);
  },

  _addClass: function(className) {
    if(className === undefined) return;
    if(this.el.classList)
      this.el.classList.add(className);
    else
      this.el.className += ' ' + className;
  },

  resizeOverlay: function() {
    var width = L.Device.windowWidth;
    var height = L.Device.windowHeight;

    switch(L.settings.layout.scaling) {
      case "stretch":
      case "fill":
        this.el.style.width = width +"px";
        this.el.style.height = height + "px";
        break;
      case "fit":
        this.el.style.left = "50%";
        this.el.style.maxWidth = "100vw";
        this.el.style.maxHeight = "100vh";
        var orientation = L.Device.orientation;
        if(L.Device.mobile === false) {
          if((width / height) >= (L.settings.layout.width / L.settings.layout.height))
            orientation = "landscape";
          else
            orientation = "portrait";
        }
        if(orientation == "landscape") {
          var scale = height / L.settings.layout.height;
          var scaledWidth = scale * L.settings.layout.width
          this.el.style.marginLeft = -0.5 * scaledWidth + "px";
          this.el.style.width = scaledWidth + "px";
          this.el.style.height = height + "px";
        } else {
          var scale = width / L.settings.layout.width;
          this.el.style.marginLeft = -0.5 * width + "px";
          this.el.style.width = width + "px";
          this.el.style.height = scale * L.settings.layout.height + "px";
        }
        break;
    }
  },
});
