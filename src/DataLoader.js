var Class = require("class-js2");
var EventEmitter = require("eventemitter3");
var _ = require("underscore");

module.exports = Class.create({
  mixins: [EventEmitter],


  constructor: function() {
    EventEmitter.call(this);
    this.dataCount = 0;
    this.loadedData = 0;
  },

  load: function(fileList, cb) {
    // fresh counters - NOT getting rid of loaded data
    this.dataCount = 0;
    this.loadedData = 0;

    cb = cb || function() {};
    this.dataCount += fileList.length;

    L.PIXI.loader.on("progress", _.bind(this.onLoad, this));
    L.PIXI.loader.on("complete", _.bind(this.onComplete, this));
    for(i in fileList) {
      L.PIXI.loader.add(fileList[i], fileList[i]);
    }
    L.PIXI.loader.load(cb);
  },

  onLoad: function(e) {
    this.loadedData++;
    
    this.emit("onload", this.status());
  },

  onComplete: function() {
    this.emit("loadcomplete");

    setTimeout(function() {
      L.PIXI.loader.off("progress");
      L.PIXI.loader.off("complete");
    }, 100);
  },

  status: function() {
    return {
      total: this.dataCount,
      complete: this.loadedData
    };
  },
});
