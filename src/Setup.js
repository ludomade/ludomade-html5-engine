module.exports = function(opts) {
  if(opts.settings && !opts.settings.debug) console.log = function(){};
  L.settings = opts.settings;
  L.settings.layout = L.settings.layout || {};
  L.settings.layout.scaling = L.settings.layout.scaling || "fill";

  L.settings.defaults = L.settings.defaults || {};
  L.settings.defaults.volume = L.settings.defaults.volume || 1;

  // Priority Lib:
  L.Device = new L.Device();
  L.PixiRenderer = new L.PixiRenderer();
  L.SceneManager = new L.SceneManager();
  // Remaining Lib:
  L.Analytics = new L.Analytics();
  L.FontLoader = new L.FontLoader();
  L.DataLoader = new L.DataLoader();
  L.Input = new L.Input();
  L.Sound = new L.Sound();
  L.Sprite = new L.Sprite();
  L.Storage = new L.Storage();
  L.Utils = new L.Utils();
  L.Spine = new L.Spine();

  L.I18n = L.I18n();
  // Inject preload requirements
  L.I18n.setup(opts);

  // Scenes and overlays:
  L.preloaderScene = new L.preloaderScene();

  if(opts.preload) {
    for(k in opts.preload) {
      switch(k) {
        case "data":
          L.DataLoader.load(opts.preload[k], function(loader, resources) {
            L.I18n.loadTranslationStrings(resources);
          });
          break;
        case "font":
          L.FontLoader.load(opts.preload[k]);
          break;
        case "audio":
          L.Sound.load(opts.preload[k]);
          break;
        default:
          throw new Error("Unsure how to load '" + k + "' assets...");
      }
    }
  }
}
