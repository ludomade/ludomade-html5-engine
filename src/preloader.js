var Overlay = require("./Overlay.js");

module.exports = Overlay.extend({
	name: "preloader",

	setup: function() {
		this.el.style.textAlign = "center";
		this.barContainer = document.createElement("div");
		this.progressBar = document.createElement("div");
	},

	onAdd: function() {
		this.barContainer.appendChild(this.progressBar);

		var borderSize = this.el.offsetHeight * 0.005;
		this.barContainer.style.position = "relative";
		this.barContainer.style.display = "inline-block";
		this.barContainer.style.top = "40%";
		this.barContainer.style.width = "60%";
		this.barContainer.style.height = "5%";
		this.barContainer.style.border = borderSize + "px solid white";

		this.progressBar.style.height = "100%";
		this.progressBar.style.width = "0%";
		this.progressBar.style.backgroundColor = "#ffbf00";
		this.progressBar.style.transition = "width 0.3s linear";

		this.el.appendChild(this.barContainer);
		this.updatePercent(0);
	},

	onRemove: function() {
		this.el.removeChild(this.barContainer);
	},

	updatePercent: function(percent) {
		this.progressBar.style.width = percent + "%";
	}
});
