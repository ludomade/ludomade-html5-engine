var Class = require("class-js2");
var PIXI = require("pixi.js");
var _ = require("underscore");

module.exports = Class.create({
  constructor: function(sheetName, frameData) {
    this.frameCounter = 0;
    this.numberOfFrames = 0;
    this.currentSequence = undefined;
    this.timerInterval = undefined;
    this.playing = false;

    this.repeater = undefined;
    this.sheetTexture = undefined;
    this.animationFrames = [];
    this.clip = undefined;
    this.animations = undefined;

    this.sheetTexture = PIXI.Texture.fromImage(sheetName);
    this.animations = frameData.animations;

    var fWidth = frameData.frame.width;
    var fHeight = frameData.frame.height;

    var framesWide = parseInt(this.sheetTexture.width/fWidth);
    var framesTall = parseInt(this.sheetTexture.height/fHeight);

    var x = y = 0;
    while(y < framesTall) {
      while(x < framesWide) {
        this.animationFrames.push(new PIXI.Texture(this.sheetTexture, new PIXI.Rectangle(fWidth * x, fHeight * y, fWidth, fHeight)));
        x++;
      }
      y++;
      x = 0;
    }

    this.clip = new PIXI.extras.MovieClip(this.animationFrames);

    this.clip.show = _.bind(this.show, this);
    this.clip.playSequence = _.bind(this.playSequence, this);

    window.anims = this.animations;
    window.thing = this;
  },
      
  show: function(frameNumber) {
    this.reset();
    this.clip.gotoAndStop(frameNumber);
  },

  reset: function() {
    if (this.timerInterval !== undefined && this.playing === true) {
      this.playing = false;
      this.frameCounter = 0;
      this.startFrame = 0;
      this.endFrame = 0;
      this.numberOfFrames = 0;
      clearInterval(this.timerInterval);
    }
  },

  playSequence: function(sequenceName, loop, repeater) {
    if(this.currentSequenceName == sequenceName) return;
    this.currentSequenceName = sequenceName;

    loop = !!loop;
    this.reset();

    this.clip.loop = loop;

    if(!!repeater)
      this.repeater = sequenceName;

    this.currentSequence = this.animations[sequenceName];
    this.numberOfFrames = this.currentSequence.length;

    if (!this.clip.fps) this.clip.fps = 12;
    var frameRate = 1000 / this.clip.fps;

    this.clip.gotoAndStop(this.currentSequence[0]);

    if(!this.playing) {
      this.timerInterval = setInterval(_.bind(this.advanceFrame, this), frameRate);
      this.playing = true;
    }
  },

  stopSequence: function() {
    this.currentSequenceName = undefined;
    clearInterval(this.timerInterval);
    this.playing = false;
  },

  advanceFrame: function() {
    this.frameCounter += 1;
    if (this.frameCounter < this.numberOfFrames) {
      this.clip.gotoAndStop(this.clip.currentFrame + 1);
    } else {
      if (this.clip.loop) {
        this.clip.gotoAndStop(this.currentSequence[0]);
        this.frameCounter = 0;
      } else if(this.repeater !== undefined) {
        this.playSequence(this.repeater);
      } else {
        this.currentSequenceName = undefined;
      }
    }
  }
});
