var PIXI = require("pixi.js");
var Layer = require("./Layer.js");

// WE DON'T KEEP REFS FOR SPRITES! FIX!!!
module.exports = Layer.extend({
  repeaters: [],
  constructor: function(json) {
    this._super();

    var map = json;
    this.tileWidth = map.tilewidth;
    this.tileHeight = map.tileheight;
    this.tw = map.width;
    this.th = map.height;
    // PROPERTIES

    this.parseTileSets(map);
    this.parseLayers(map);
    this.buildMap();
  },

  buildMap: function() {
    var len = this.layers.length;
    while(len--) {
      switch(this.layers[len].type) {
        case "tilelayer":
          this.renderTileLayer(this.layers[len]);
          break;
        case "objectgroup":
          this.renderObjectLayer(this.layers[len]);
          break;
        case "imagelayer":
          this.renderImageLayer(this.layers[len]);
          break;
      }
    };
  },

  renderImageLayer: function(layer) {
    var imagePath = layer.image.split("/");
    imagePath = "images/" + imagePath[imagePath.length - 1];
    var texture = PIXI.Texture.fromImage(imagePath);
    var repeater = new PIXI.extras.TilingSprite(texture, texture.width, texture.height);
    repeater.width = L.PixiRenderer.renderer.width;
    repeater.height = L.PixiRenderer.renderer.height;
    repeater.tilePosition.x = 0;
    repeater.tilePosition.y = 0;
    repeater.speedX = layer.properties.speedX;
    repeater.speedY = layer.properties.speedY;
    layer.renderable = this.repeater;
    this.addChild(repeater);
    this.repeaters.push(repeater);
  },

  renderTileLayer: function(layer) {
    layer.sprites = [];
    var len = layer.data.length;
    var position = 0;
    do {
      if(layer.data[position] === 0 || layer.data[position] === undefined) continue;
      var tLen = this.tileSets.length;
      var tiles = undefined;
      while(tLen--) {
        if(this.tileSets[tLen].firstGid <= layer.data[position] && (tiles === undefined || tiles.firstGid < this.tileSets[tLen].firstGid))
          tiles = this.tileSets[tLen];
      }

      layer.tWidth = tiles.tileWidth;
      layer.tHeight = tiles.tileHeight;

      var cols = tiles.imageWidth / tiles.tileWidth;
      var rows = tiles.imageHeight / tiles.tileHeight;

      var sheetIdx = layer.data[position] - tiles.firstGid;

      var col = sheetIdx % cols;
      var row = Math.floor(sheetIdx / cols);

      var frame = new PIXI.Rectangle(col * tiles.tileWidth, row * tiles.tileHeight, tiles.tileWidth, tiles.tileHeight);
      var texture = new PIXI.Texture(tiles.baseTexture, frame);

      var sprite = new PIXI.Sprite(texture);
      sprite.position.x = (position % layer.width) * tiles.tileWidth;
      sprite.position.y = Math.floor(position / layer.width) * tiles.tileHeight;

      this.addChild(sprite);
      layer.sprites.push(sprite);
    } while(position++ < len);

    this.collisionLayer = layer;
  },

  renderObjectLayer: function(layer) {
    layer.sprites = [];
    var len = layer.objects.length;
    while(len--) {
      var tLen = this.tileSets.length;
      var tiles = undefined;
      while(tLen--) {
        if(this.tileSets[tLen].firstGid <= layer.objects[len].gid && (tiles === undefined || tiles.firstGid < this.tileSets[tLen].firstGid))
          tiles = this.tileSets[tLen];
      }

      var cols = tiles.imageWidth / tiles.tileWidth;
      var rows = tiles.imageHeight / tiles.tileHeight;

      var sheetIdx = layer.objects[len].gid - tiles.firstGid;

      var tileProps = _.extend(tiles.properties, tiles.tileProperties["" + sheetIdx +""]);
      tileProps = _.extend(layer.objects[len], tileProps);
      var Clazz = require("game/" + tileProps.Class);

      var obj = new Clazz(this, tileProps);
      obj.clazz = tileProps.Class;

      this.addChild(obj.sprite);
      layer.sprites.push(obj);
    }
  },

  followFirst: function(clazz, opts) {
    this.followSprite = this.find(clazz)[0];
    this.followSpriteOpts = opts || {};
  },

  find: function(clazz) {
    var entities = []
    var len = this.layers.length;
    while(len--) {
      if(this.layers[len].type == "objectgroup") {
        var sprs = this.layers[len].sprites.length;
        while(sprs--) {
          if(this.layers[len].sprites[sprs] && this.layers[len].sprites[sprs].clazz == clazz) {
            entities.push(this.layers[len].sprites[sprs]);
          }
        }
      }
    };
    return entities;
  },

  update: function() {
    var len = this.layers.length;
    while(len--) {
      if(this.layers[len].type == "objectgroup") {
        var sprs = this.layers[len].sprites.length;
        while(sprs--) {
          if(this.layers[len].sprites[sprs] && this.layers[len].sprites[sprs].update) {
            var spr = this.layers[len].sprites[sprs];

            if(!spr.seen &&
                spr.sprite.position.x > -this.position.x &&
                spr.sprite.position.x < -this.position.x + L.PixiRenderer.renderer.width &&
                spr.sprite.position.y > -this.position.y &&
                spr.sprite.position.y < -this.position.y + L.PixiRenderer.renderer.height)
              spr.seen = true

            spr.update()

            if(spr.sprite.position.x < 0 ||
                spr.sprite.position.y < 0 ||
                spr.sprite.position.x > (this.tw * this.tileWidth) ||
                spr.sprite.position.y > (this.th * this.tileHeight)) {
              spr.emit("offmap");
            }

            L.Utils.collisions([spr.sprite], this.collisionLayer.sprites, _.bind(this.tileCollision, this));
          }
        }
      }
    }
    if(this.followSprite) {
      if(this.followSpriteOpts.followX || this.followSpriteOpts.followX === undefined) {
        this.position.x = (L.PixiRenderer.renderer.width / 2) -  this.followSprite.sprite.position.x;
      }
      if(this.followSpriteOpts.followY || this.followSpriteOpts.followY === undefined) {
        this.position.y = (L.PixiRenderer.renderer.height / 2) -  this.followSprite.sprite.position.y;
      }

      var len = this.repeaters.length;
      while(len--) {
        if(this.followSpriteOpts.followX || this.followSpriteOpts.followX === undefined) {
          this.repeaters[len].position.x = -this.position.x;
          if(this.repeaters[len].speedX && this.repeaters[len].speedX !== 0)
            this.repeaters[len].tilePosition.x = this.position.x * this.repeaters[len].speedX;
        }
        if(this.followSpriteOpts.followY || this.followSpriteOpts.followY === undefined) {
          this.repeaters[len].position.y = -this.position.y;
          if(this.repeaters[len].speedY && this.repeaters[len].speedY !== 0)
            this.repeaters[len].tilePosition.y = this.position.y * this.repeaters[len].speedY;
        }
      }
    }
  },

  tileCollision: function(spr, tile) {
    var sprCenterX = (spr.scale.x * spr.width/2) + (spr.position.x - (spr.scale.x * spr.width * spr.anchor.x));
    var sprCenterY = (spr.scale.y * spr.height/2) + (spr.position.y - (spr.scale.y * spr.height * spr.anchor.y));
    var tileCenterX = (tile.width/2) + (tile.position.x - (tile.width * tile.anchor.x));
    var tileCenterY = (tile.height/2) + (tile.position.y - (tile.height * tile.anchor.y));

    var hd = Math.abs(sprCenterX - tileCenterX);
    var vd = Math.abs(sprCenterY - tileCenterY);

    if(spr.scale.x * spr.width > spr.scale.y * spr.height)
      vd = vd * Math.abs(spr.width/spr.height);
    else
      hd = hd * Math.abs(spr.width/spr.height);

    if(vd > hd) {
      // Bottom
      if(sprCenterY < tileCenterY) {
        spr.velocity.y = 0;
        spr.position.y = tile.position.y - (tile.height * tile.anchor.y) - (spr.scale.y * spr.height * (1-spr.anchor.y));
        spr.landed = true;
        spr.emit("bump.bottom", tile);
      } else {
        spr.velocity.y = 0;
        spr.position.y = tile.position.y + (tile.height * (1-tile.anchor.y)) + (spr.scale.y * spr.height * spr.anchor.y);
        spr.emit("bump.top", tile);
      }
    } else {
      // Left
      if(sprCenterX > tileCenterX) {
        spr.velocity.x = 0;
        spr.position.x = tile.position.x + (tile.width * (1-tile.anchor.x)) + (spr.scale.x * spr.width * spr.anchor.x);
        spr.emit("bump.left", tile);
      } else if(sprCenterX < tileCenterX) {
        spr.velocity.x = 0;
        spr.position.x = tile.position.x - (tile.width * tile.anchor.x) - (spr.scale.x * spr.width * (1-spr.anchor.x));
        spr.emit("bump.right", tile);
      }
    }
  },

  parseLayers: function(map) {
    this.layers = [];
    var len = map.layers.length;
    while(len--) {
      switch(map.layers[len].type) {
        case "tilelayer":
          this.parseTileLayer(map.layers[len]);
          break;
        case "objectgroup":
          this.parseObjectLayer(map.layers[len]);
          break;
        case "imagelayer":
          this.parseImageLayer(map.layers[len]);
          break;
      }
    }
  },

  parseTileSets: function(map) {
    this.tileSets = [];
    var len = map.tilesets.length;
    while(len--) {
      var tileSetData = map.tilesets[len];
      var tileSet = {};
      var imagePath = tileSetData.image.split("/");
      imagePath = "images/" + imagePath[imagePath.length - 1];
      tileSet.baseTexture = PIXI.Texture.fromImage(imagePath);
      tileSet.name = tileSetData.name;
      tileSet.firstGid = tileSetData.firstgid;
      tileSet.tileWidth = tileSetData.tilewidth;
      tileSet.tileHeight = tileSetData.tileheight;
      tileSet.imageWidth = tileSetData.imagewidth;
      tileSet.imageHeight = tileSetData.imageheight;
      tileSet.properties = tileSetData.properties;
      tileSet.tileProperties = tileSetData.tileproperties;
      this.tileSets.push(tileSet);
    }
  },

  // In case we need to modify them!
  parseTileLayer: function(layerData) { this.layers.push(layerData); },
  parseObjectLayer: function(layerData) { this.layers.push(layerData); },
  parseImageLayer: function(layerData) { this.layers.push(layerData); },
});
