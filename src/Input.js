var Class = require("class-js2");
var Mousetrap = require("mousetrap");

module.exports = Class.create({
  action: {},

  constructor: function() {
    // MousetrapPause(Mousetrap);
  },
  bindKey: function(bind, callback, evtType) {
    // Mousetrap.bind(bind, callback, evtType);
    // Mousetrap recommends not passing event type.
    if(evtType !== undefined)
      Mousetrap.bind(bind, callback, evtType);
    else
      Mousetrap.bind(bind, callback);
  },
  unbindKey: function(bind) {
    Mousetrap.unbind(bind);
  },
  pauseKeys: function() {
    Mousetrap.pause();
  },
  resumeKeys: function() {
    Mousetrap.unpause();
  },
  clearAllKeys: function() {
    Mousetrap.reset();
  },

  bindAction: function(key, action) {
    if(action === undefined) action = key;

    this.action[action] = false;
    var setter = (function(action) {
      return function() {
        L.Input.action[action] = true;
      }
    }(action))
    var unsetter = (function(action) {
      return function() {
        L.Input.action[action] = false;
      }
    }(action))
    this.bindKey(key, setter, "keydown");
    this.bindKey(key, unsetter, "keyup");
  },

  unbindAction: function(key, action) {
    delete this.action[action];
    this.unbindKey(key);
  }
});
