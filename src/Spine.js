var Class = require("class-js2");
var spine = require("pixi-spine");

// Private Wrapper
var spineWrapper = Class.create({
  constructor: function(spineData) {
    this.graphics = new spine.Spine(spineData);
  },

  setSkin: function(name) {
		this.graphics.skeleton.setSkinByName(name);
		this.graphics.skeleton.setSlotsToSetupPose();
  },

  setAnimationByName: function(priority, animName, loop) {
		this.graphics.state.setAnimationByName(priority, animName, loop);
  },

  clearAnimation: function() {
		this.graphics.state.clearTracks();
  },

  addToContainer: function(container) {
    container.addChild(this.graphics);
  },

  removeFromContainer: function(container) {
    container.removeChild(this.graphics);
  }
});


// Exports

module.exports = Class.create({
  fromSpineData: function(spineData) {
    return new spineWrapper(spineData);
  },

  loadJson: function(name, cb) {
    // Should defer? look to load batches?
		if(!L.PIXI.loader.resources[name]) L.PIXI.loader.add(name, 'images/' + name + '.json');
		L.PIXI.loader.load(this._onAssetsLoaded.bind(this, name, cb));
  },

	_onAssetsLoaded: function(name, cb, loader, res) {
    if(res[name].spineData) {
      cb(this.fromSpineData(res[name].spineData));
    } else {
      throw new Error("Could not load " + name + ".json or the associated files");
    }
	},
});
