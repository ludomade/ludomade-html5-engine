var I18n = require("i18n-js");

module.exports = function() {
  I18n.setup = function(opts) {
    if(typeof window.I18n == "object") {
      I18n.locale = window.I18n.locale || I18n.locale;
      I18n.defaultLocale = window.I18n.defaultLocale || I18n.defaultLocale;
    }

    opts.preload = opts.preload || {};
    opts.preload.data = opts.preload.data || [];

    opts.preload.data.push("data/" + I18n.locale + ".json");
    if(I18n.locale !== I18n.defaultLocale) opts.preload.data.push("data/" + I18n.defaultLocale + ".json");
  };

  I18n.loadTranslationStrings = function(resources) {
    I18n.translations = I18n.translations || {};
    for(k in resources) {
      if((k == "data/" + I18n.defaultLocale + ".json" || k == "data/" + I18n.locale + ".json") && resources[k].error){
        throw new Error(resources[k].error);
      }
      if(k == "data/" + I18n.locale + ".json" && !resources[k].error){
        I18n.translations[I18n.locale] = resources[k].data;
      }
      if(k == "data/" + I18n.defaultLocale + ".json" && !resources[k].error && I18n.locale !== I18n.defaultLocale){
        I18n.translations[I18n.defaultLocale] = resources[k].data;
      }
    }
  };

  return I18n;
};
// Setup/Use: https://github.com/fnando/i18n-js#setting-up
// Basic structure: https://github.com/fnando/i18n-js#using-i18njs-with-other-languages-python-php-

// Silly? Maybe... but leaves room for Monkey-Patches. - Sartin 8/21/15
