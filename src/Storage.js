var Class = require("class-js2");

var memStore = Class.create({
  constructor: function() {
    this.storage = {};
  },
  getItem: function(key) {
    if(key in this.storage) return this.storage[key];
    else return null;
  },
  setItem: function(key, value) {
    return this.storage[key] = value;
  },
  removeItem: function(key) {
    delete this.storage[key];
  },
  clear: function() {
    this.storage = {};
  },
});

module.exports = Class.create({
  constructor: function() {
    if(this.canLocalStore()) {
      this.store = localStorage;
    } else {
      this.store = new memStore();
    }
  },
  get: function(key) {
    return this.store.getItem(key);
  },
  set: function(key, value) {
    this.store.setItem(key, value);
  },
  remove: function(key) {
    this.store.removeItem(key);
  },
  reset: function(verify) {
    if(verify) this.store.clear();
  },
  canLocalStore: function() {
    var mod, result;
    try {
      mod = new Date;
      localStorage.setItem(mod, mod.toString());
      result = localStorage.getItem(mod) === mod.toString();
      localStorage.removeItem(mod);
      return result;
    } catch (_error) {
      return false;
    }
  }
});
