var Class = require("class-js2");
var EventEmitter = require("eventemitter3");
var Howler = require("howler");
var _ = require("underscore");

module.exports = Class.create({
  mixins: [EventEmitter],

  music: undefined,
  loadedSounds: 0,
  files: [],
  soundCache: {},
  muted: false,

  constructor: function() {
    EventEmitter.call(this);
    this.player = Howler.Howler;
    this.setVolume(L.settings.defaults.volume);
  },

  load: function(fileList) {
    this.files = this.files.concat(fileList);
    for(i in fileList) {
      var name = fileList[i].fileName;
      var id = fileList[i].id || name;

      var urls = [];
      for(x in fileList[i].types) {
        urls.push("audio/" + name + "." + fileList[i].types[x]);
      }

      this.soundCache[id] = new Howl({
        src: urls,
        volume: fileList[i].volume || 1.0,
        onload: _.bind(this.onLoad, this),
      });
    }
  },
  onLoad: function() {
    this.loadedSounds++;

    this.emit("onload", this.status());
    if(this.loadedSounds == this.files.length) {
      this.onComplete();
    }
  },
  onComplete: function() {
    this.emit("loadcomplete");
    // NOOP - bind later/elsewhere?
  },

  play: function(soundID, opts) {
    this.soundCache[soundID].play();
    return this.soundCache[soundID];
  },

  sel: function(soundID) {
    return this.soundCache[soundID];
  },

  setMusic: function(soundID) {
    this.stopMusic();
    this.music = this.sel(soundID);
    this.resumeMusic();
  },

  pauseMusic: function() { if(this.music) this.music.pause(); },
  resumeMusic: function() {
    if(this.music && !this.music.playing()) {
      this.music.play();
      this.music.loop(true);
    }
  },
  stopMusic: function() { if(this.music) this.music.stop(); },

  status: function() {
    return {
      total: this.files.length,
      complete: this.loadedSounds
    };
  },

  mute: function(state) {
    this.muted = state === undefined ? true : state;
    this.player.mute(this.muted);
  },

  setVolume: function(vol) {
    this.player.volume(vol);
  },
  getVolume: function() {
    return this.player.volume();
  },

  unloadSound: function(id) {
    this.soundCache[id].unload();
  },
});
