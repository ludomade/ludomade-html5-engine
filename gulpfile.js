var source = require('vinyl-source-stream');
var streamify = require('gulp-streamify');
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var gulp = require('gulp');
var derequire = require('gulp-derequire');

gulp.task('build', function() {
  var bundleStream = browserify({entries:['./src/main.js'], standalone: "LGE"}).bundle()
 
  bundleStream
    .pipe(source('src/main.js'))
    .pipe(derequire())
    .pipe(rename('lge.js'))
    .pipe(gulp.dest('./dist'))
    .pipe(streamify(uglify()))
    .pipe(rename('lge.min.js'))
    .pipe(gulp.dest('./dist'))
});
